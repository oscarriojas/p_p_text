# -*- coding: utf-8 -*-
"""
Created on Thu Aug  4 23:06:27 2022

@author: ejemp
"""
# importando la librería de esta forma no se generan errores
import p_p_text.clean_text as ct

T1 = 'Un perro necesita salir a caminar 125'

T2 = ct.CleanText(T1).c_numbers(not_clear=False)
print(T2)
